FROM debian:latest

RUN apt update && apt install ca-certificates curl gnupg lsb-release -y

RUN  curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

RUN  curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o /usr/bin/docker-compose

RUN chmod +x /usr/bin/docker-compose

RUN echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
| tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt update && apt install docker-ce docker-ce-cli containerd.io -y

COPY entrypoint.sh /

CMD ["/entrypoint.sh"]
